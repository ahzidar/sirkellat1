<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('id', 'desc')
        ->get();
        foreach ($users as $user) {
          $user->view_user = [
            'href' => 'api/v1/register/' . $user->id,
            'method' => 'GET'
          ];
        }

        $response = [
            'msg' => 'List of All Users',
            'ver' => '1',
            'data' => $users
          ];
    
          return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
        'name' => 'required',
        'email' => 'required'
        ]);

        $name = $request->name;
        $email = $request->email;
        $date_birth = $request->input('date_birth');
        $address = $request->input('address');
        $last_education = $request->input('last_education');
        $major = $request->input('major');
        $role_id = $request->input('role_id');
        $password = bcrypt($request->input('password'));
        
        
        // Store to Users
        $users = new User;
        $users->name = $name;
        $users->email = $email;
        $users->date_birth = $date_birth;
        $users->address = $address;
         // Store CV to Folder Public/CV
         if($request->hasFile('cv')){
            $cv = $request->cv;
            $uploadPath = public_path('/cv');
            $extension = $cv->getClientOriginalExtension();
            $cvName = date("Y-m-d-his").str_slug($request->name).'.'.$extension;
            $cv->move($uploadPath, $cvName);
            $users->cv = '/cv/' . $cvName;
        } 

        $users->last_education = $last_education;
        $users->major = $major;
        $users->role_id = $role_id;
        $users->password = $password;
        $users->save();  

        $message = [
            'msg' => 'Users has been created',
            'ver' => '1',
            'users' => $users
        ];
        return response()->json($message, 201);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $users = User::find($id);
        $users->name = $request->name;
        $users->email = $request->email;
        $users->date_birth = $request->date_birth;
        $users->address = $request->address;
         // Store CV to Folder Public/CV
         if($request->hasFile('cv')){
            $cv = $request->cv;
            $uploadPath = public_path('/cv');
            $extension = $cv->getClientOriginalExtension();
            $cvName = date("Y-m-d-his").str_slug($request->name).'.'.$extension;
            $cv->move($uploadPath, $cvName);
            $users->cv = '/cv/' . $cvName;
        } 

        $users->last_education = $request->last_education;
        $users->major = $request->major;
        $users->role_id = $request->role_id;
        $users->password = bcrypt($request->password);
        $users->save();  

        $message = [
            'msg' => 'Users has been updated',
            'ver' => '1',
            'users' => $users
        ];
        return response()->json($message, 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $users = User::find($id);
        
        if(is_null($users)){
            $message = [
                'msg' => 'Users not found'
            ];
            return response()->json($message, 200);
        } else {
            if($users->delete()) {
                $message = [
                    'msg' => 'Users has been deleted'
                ];
                return response()->json($message, 200);
            } else {
                $message = [
                    'msg' => 'Users has not been deleted'
                ];
                return response()->json($message, 200);
            }
        }

        

        
    }
}
