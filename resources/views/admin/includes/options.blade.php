<li class="nav-item has-treeview">
    <a href="#" class="nav-link">
      <i class="nav-icon fa fa-pie-chart"></i>
      <p>
        Options
        <i class="right fa fa-angle-left"></i>
      </p>
    </a>
    <ul class="nav nav-treeview">
      <li class="nav-item">
        <a href="pages/charts/chartjs.html" class="nav-link">
          <i class="fa fa-circle-o nav-icon"></i>
          <p>Sliders</p>
        </a>
      </li>
      <li class="nav-item">
        <a href="pages/charts/flot.html" class="nav-link">
          <i class="fa fa-circle-o nav-icon"></i>
          <p>Information</p>
        </a>
      </li>
      
    </ul>
  </li>