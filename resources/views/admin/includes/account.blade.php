<li class="nav-item has-treeview menu-open">
        <router-link to="/admin" class="nav-link active">
      <i class="nav-icon fa fa-dashboard"></i>
      <p>
        Account Management
        <i class="right fa fa-angle-left"></i>
      </p>
        </router-link>
    <ul class="nav nav-treeview">
      <li class="nav-item">
        <router-link to="/admin" class="nav-link active">
            {{-- <a href="/admin" class="nav-link active"> --}}
            <i class="fa fa-circle-o nav-icon"></i>
            <p>Users Admin</p>
            {{-- </a> --}}
        </router-link>
      </li>
      <li class="nav-item">
        <a href="/index2.html" class="nav-link">
          <i class="fa fa-circle-o nav-icon"></i>
          <p>Training Member</p>
        </a>
      </li>
      <li class="nav-item">
        <a href="./index3.html" class="nav-link">
          <i class="fa fa-circle-o nav-icon"></i>
          <p>Freelancers</p>
        </a>
      </li>
    </ul>
  </li>